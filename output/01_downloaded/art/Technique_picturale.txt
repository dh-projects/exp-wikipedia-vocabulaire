Une technique picturale est une technique qui contribue à la réalisation d'une peinture ou d'une image qui évoque la peinture.
L'expression technique picturale décrit « un procédé opératoire conscient, réglé, reproductible et transmissible » des arts plastiques, en particulier la matière et les instruments que l'artiste a utilisés, qui vont de la fresque à l'aquarelle et en passant par la peinture à l'huile et le pastel, ainsi que leurs procédés d'application.
Les techniques picturales comprennent principalement la composition, qui concerne la photographie comme la peinture, le dessin, préparatoire ou sous-jacent, impliquant la perspective, en plus des méthodes d'application des couleurs.


== Techniques ==
Parmi les principales techniques, on peut citer :

la fresque ou peinture à la chaux pratiquée depuis l'Antiquité et redécouverte à la Renaissance ;
la tempera, apparue au Moyen Âge ;
la peinture à l'huile (apparue à la fin du Moyen Âge) ;
l'aquarelle, longtemps technique d'étude, devenue progressivement à partir de la fin du XVIIIe siècle une technique picturale à part entière ;
la gouache, variante opaque de l'aquarelle, beaucoup pratiquée en milieu scolaire ;
la peinture acrylique et la peinture vinylique (apparues au début des années 1950) ;
la peinture aérosol utilisée dans l'art moderne.Trois éléments sont à la base de chacune de ces techniques :

les pigments qui définissent l'étendue de la palette colorée ;
le liant qui diffère selon la technique et définit sa spécificité ;
le support de peinture qui doit être adapté à chaque technique selon sa nature et ses compatibilités.Ces dénominations techniques basées sur le liant et le support des peintures sont largement utilisées, avec les classes de genre (comme paysage, marine, portrait) pour la classification sommaire des œuvres d'art. Il arrive fréquemment que des œuvres utilisent conjointement plusieurs techniques ; dans la peinture classique, on trouve de l'huile sur tempera ; des rehauts de gouache sur de l'aquarelle, etc. Dans l'art moderne, on connaît en outre des œuvres picturales réalisées par collage. Quand on ne peut déterminer une matière picturale principale, on parle de technique mixte.
D'autres techniques sont des procédés d'application, comme le glacis à la peinture à l'huile, ou l'aérographe.
Au cours de la réalisation, l'artiste met en œuvre des techniques intermédiaires, comme les études et dessins préparatoires, la mise au carreau ou le poncif pour le report sur le support définitif.


== Histoire ==
Jusqu'au XIXe siècle, « art » signifie tout ce qu'on désigne aujourd'hui par « technique », tandis que le mot technique ne s'emploie que comme adjectif. L'expression « technique picturale » est attestée en 1866.
L'évolution de la peinture est liée à l'évolution des techniques mais aussi à l'évolution de la perception du monde par les hommes[réf. souhaitée].
Certains procédés picturaux sont étroitement liés à l'artiste qui les a inventés. Ce sont des techniques picturales s'ils ont le caractère d'une méthode transmissible. Ils deviennent la marque d'un artiste ou d'une époque. Signac revendique ainsi pour le divisionnisme la qualité de technique picturale : « il s'agit de technique et non de talent ».


== Voir aussi ==


=== Bibliographie ===
Ségolène Bergeon-Langle et Pierre Curie, Peinture et dessin, Vocabulaire typologique et technique, Paris, Editions du patrimoine, 2009, 1249 p. (ISBN 978-2-7577-0065-5).


=== Articles connexes ===
Peinture (matière)
Matériel de peinture
Composition picturale
Pochoir


=== Notes et références ===

 Portail de la peinture