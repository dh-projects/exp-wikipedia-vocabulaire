Plusieurs planètes ou autres corps membres d'un système planétaire sont connus pour posséder, ou avoir possédé, un champ magnétique.


== Historique ==
La Boussole est connue en Chine depuis l'antiquité (la date précise de son invention reste inconnue) et introduite (ou réinventée) en Europe et au Moyen-orient vers 1200. Aucune théorie explicative n'existe alors sur son fonctionnement : les sources médiévales se contentent de constater que la boussole indique toujours la direction de l'étoile polaire.
En 1600, la notion de champ magnétique terrestre fait ses premiers pas avec William Gilbert, qui réalise une maquette appelée Terrella, qui est une sphère aimantée. Il montre qu'une boussole placée sur la terrella indique toujours le même point. Il a ainsi compris que la terre possède un champ magnétique global. Pour l'expliquer, il propose que l'ensemble de la planète possède une aimantation permanente, similaire à celle d'un minéral comme la magnétite. En réalité, l'aimantation permanente de minéraux ne peut expliquer le champ magnétique à l'échelle d'une planète : les minéraux aimantables sont plutôt rares, et ne gardent leur cohérence qu'à des températures modestes (sous leur température de Curie), donc seulement près de la surface d'une planète comme la Terre.


== Modèle de la dynamo planétaire ==


=== Présentation générale ===
Le modèle dynamo, actuellement admis, a été développé par Walter M. Elsasser.
Il attribue la génération du champ magnétique à des mouvements de fluides conducteurs : il s'agit de la partie liquide du noyau ferreux (noyau externe) pour les planètes telluriques, et de l'hydrogène métallique dans le cas des planètes gazeuses. Le champ magnétique d'une planète est un phénomène auto-entretenu : ainsi, de façon contre-intuitive, il est nécessaire de le prendre en compte pour comprendre les phénomènes qui lui donnent naissance.
Ces fluides conducteurs sont soumis à plusieurs effets : 

des effets de convection thermique, correspondance à un transport de chaleur vers l'extérieur ;
des effets de convection chimique (différence de composition) peuvent s'y ajouter ;
la force de Coriolis due à la rotation de la planète, tend à faire tourbillonner leur mouvement ;
l’apparition d'une force électromotrice, car il se déplace dans le champ magnétique (induction). Il est donc siège d'un courant électrique ;
en conséquence, il subit aussi des forces de Lorentz.Un équilibre dynamique s’établit entre ces différentes forces, ce qui se traduit par des courants hélicoïdaux. Les courants induits entretiennent le champ magnétique. 


=== Equations principales ===
L'écoulement est représenté par une équation de Navier Stokes à laquelle s'ajoute le terme de la force de Laplace  :

  
    
      
        
          
            
              δ
              
                
                  
                    v
                    →
                  
                
              
            
            
              δ
              t
            
          
        
        +
        
          
            
              v
              →
            
          
        
        ⋅
        
          
            
              Δ
              →
            
          
        
        
          
            
              v
              →
            
          
        
        =
        −
        
          
            1
            ρ
          
        
        
          
            
              Δ
              →
            
          
        
        P
        +
        ν
        Δ
        
          
            
              v
              →
            
          
        
        +
        
          
            
              j
              →
            
          
        
        ∧
        
          
            
              B
              →
            
          
        
        +
        
          
            
              f
              →
            
          
        
      
    
    {\displaystyle {\frac {\delta {\vec {v}}}{\delta t}}+{\vec {v}}\cdot {\vec {\Delta }}{\vec {v}}=-{\frac {1}{\rho }}{\vec {\Delta }}P+\nu \Delta {\vec {v}}+{\vec {j}}\wedge {\vec {B}}+{\vec {f}}}
  
Où :

  
    
      
        
          
            
              v
              →
            
          
        
      
    
    {\displaystyle {\vec {v}}}
   est la vitesse.

  
    
      
        P
      
    
    {\displaystyle P}
   est le tenseur des contraintes.

  
    
      
        v
        e
        c
        
          B
        
      
    
    {\displaystyle vec{B}}
   est le champ magnétique.Le champ magnétique étant pour sa part commandé par l'équation : 

  
    
      
        
          
            
              δ
              
                
                  
                    B
                    →
                  
                
              
            
            
              δ
              t
            
          
        
        =
        
          
            1
            
              
                μ
                
                  0
                
              
              σ
            
          
        
        
          ∇
          
            2
          
        
        
          
            
              B
              →
            
          
        
        +
        Δ
        ×
        
          (
          
            
              
                
                  v
                  →
                
              
            
            ×
            
              
                
                  B
                  →
                
              
            
          
          )
        
      
    
    {\displaystyle {\frac {\delta {\vec {B}}}{\delta t}}={\frac {1}{\mu _{0}\sigma }}\nabla ^{2}{\vec {B}}+\Delta \times \left({\vec {v}}\times {\vec {B}}\right)}
  


=== Effets α et Ω ===


=== Critères ===
La théorie du modèle dynamo et la comparaison des planètes (et principales lunes) du système solaire permet de définir un certain nombre de critères qui font qu'une planète possède, ou non, un champ magnétique auto-entretenu. Le corps planétaire considéré doit posséder un noyau liquide (ou partiellement liquide), ce qui exclut d'emblée les corps non différenciés. Il faut aussi que le corps soit encore en train de se refroidir, ce qui produit des courants de convection. 
Dans les planètes dont l'intérieur est largement refroidi, comme Mars, les courants convectifs sont arrêtés, et aucune dynamo planétaire ne peut fonctionner. La présence d'une convection n'est cependant une condition suffisante. Les conditions pour l'existence d'une « dynamo » fonctionnelle dans un corps planétaire sont exprimées par le Nombre de Reynolds magnétique, une valeur minimale, probablement située entre 10 et 100, est nécessaire.


=== Un système complexe et chaotique ===
Une dynamo planétaire est un processus physique qui reste extrêmement difficile à modéliser. Il n'existe pas, en 2021, de modèle numérique complètement prédictif du fonctionnement de la « dynamo » d'un corps planétaire. D'une part, c'est un problème multiphysique, qui fait intervenir des équations relevant aussi bien de la mécanique des fluides que de l'électromagnétisme. En outre, les données sur le noyau terrestre (et a fortiori sur celui des autres planètes) restent fragmentaires : il n'existe aucun moyen d'observation directe, tout ce qui est connu du noyau terrestre est inféré des informations magnétiques, sismiques et gravimétriques. 
Une dynamo planétaire est un système chaotique, où de petites perturbations peuvent aboutir à des basculements majeurs. L'illustration la plus flagrante en est le phénomène d'inversion. Le champ magnétique terrestre s'inverse de façon récurrente : au moins 40 fois au cours des dix derniers millions d'années, mais à des intervalles de temps très irréguliers. Il s'agit d'un phénomène chaotique, où les deux positions extrêmes de l'axe magnétique de la Terre (aligné dans un sens ou dans l'autre sur l'axe de rotation) représentent des attracteurs.


=== Modèles comportementaux ===
Étant donné l'extrême complexité physique d'une dynamo planétaire, des modèles comportementaux ont été élaborés, suffisamment simples pour être mis complètement en équation ou testés en laboratoire, et capables de rendre compte de certains comportements d'une dynamo planétaire. 


==== Bullard ====

La dynamo homopolaire a été introduite par Edward Bullard, géophysicien britannique, dans les années 1950. Il s'agit d'une variante du disque de Faraday, qui, au lieu d'être placé dans l'entrefer d'un aimant permanent, est connecté en série avec une boucle qui lui est coaxiale. La mise en équation qui suit est proposée par Moffatt, qui suggère de considérer un disque segmenté pour que le courant ne puisse circuler que dans la direction radiale.
Le flux à travers le disque s'écrit, en adoptant un système de coordonnées cylindriques (z = 0 sur le disque) : 

  
    
      
        Φ
        (
        t
        )
        =
        
          ∬
          
            S
          
        
        
          B
          
            z
          
        
        (
        r
        ,
        θ
        ,
        0
        ,
        t
        )
        
        r
        
        
          d
        
        r
        
        
          d
        
        θ
      
    
    {\displaystyle \Phi (t)=\iint _{S}B_{z}(r,\theta ,0,t)\,r\,\mathrm {d} r\,\mathrm {d} \theta }
  Le champ électrique induit sur un point du disque à une distance r de l'axe est  : 

  
    
      
        
          
            E
          
        
        (
        r
        )
        =
        2
        π
        
        Ω
        
        r
        
        
          B
          
            z
          
        
      
    
    {\displaystyle {\mathcal {E}}(r)=2\pi \,\Omega \,r\,B_{z}}
  Où 
  
    
      
        Ω
      
    
    {\displaystyle \Omega }
   est la vitesse de rotation (radian/sec). 
La différence de potentiel entre le moyeu et la périphérie du disque (force électromotrice) s'écrit en intégrant le terme ci-dessus le long d'un rayon. Le résultat se simplifie en : 

  
    
      
        e
        =
        Ω
        
        Φ
      
    
    {\displaystyle e=\Omega \,\Phi }
  On introduit les termes électriques R (la résistance du circuit), L (son inductance) et M (l'inductance mutuelle entre la boucle de courant et le disque de Faraday) :

  
    
      
        L
        
          
            
              
                d
              
              i
            
            
              
                d
              
              t
            
          
        
        +
        R
        
        i
        =
        M
        
        Ω
        
        i
      
    
    {\displaystyle L{\frac {\mathrm {d} i}{\mathrm {d} t}}+R\,i=M\,\Omega \,i}
  Bien entendu, le courant existant dans le disque soumis à un champ magnétique donne lieu à une force de Lorenz, laquelle exerce un couple qui, selon le signe, va l’accélérer ou le freiner. Ce couple s'écrit : 

  
    
      
        Γ
        =
        i
        
        Φ
      
    
    {\displaystyle \Gamma =i\,\Phi }
  On a donc, en introduisant un terme d'amortissement mécanique linéaire 
  
    
      
        α
      
    
    {\displaystyle \alpha }
   :

  
    
      
        
          J
          
            Δ
          
        
        
          
            
              
                d
              
              Ω
            
            
              
                d
              
              t
            
          
        
        =
        −
        α
        
        Ω
        +
        i
        
        Φ
      
    
    {\displaystyle J_{\Delta }{\frac {\mathrm {d} \Omega }{\mathrm {d} t}}=-\alpha \,\Omega +i\,\Phi }
  Ce modèle simple permet déjà de rendre compte de certains comportement comme le caractère auto-entretenu du champ magnétique, la non-linéarité, et l'existence d'hystérésis, de bifurcations et de seuils. Un premier cas de figure est de considérer le disque tournant à une vitesse Ω constante, entraîné par un moteur externe. Tant que i et B sont nuls, le système est purement mécanique. Si une perturbation crée un courant (un champ) infinitésimal, deux cas sont possibles. Si le système est trop résistif (
  
    
      
        R
        >
        Ω
        M
      
    
    {\displaystyle R>\Omega M}
  ), le courant sera évanescent. À l'inverse, si  
  
    
      
        R
        <
        Ω
        M
      
    
    {\displaystyle R<\Omega M}
  , le courant va être amplifié et augmenter « à l'infini », dans la limite bien sûr du couple que peut fournir le moteur externe. On a donc ici une génératrice,.


==== Rikitake ====

Le modèle de Rikitake revient à coupler deux dynamos de Bullard, chacune exposée au champ magnétique généré par une boucle de courant reliée à l'autre en série. Il a été publié en 1958 et différentes variantes ont été étudiées, en ajoutant des moteurs, des termes de dissipation, etc. Il a été utilisé pour représenter certains comportements des dynamos planétaires, mais aussi stellaires. Dans l'une de ses variations, les deux disques représentent deux composantes du champ magnétique, respectivement colinéaire et orthogonale à l'axe de rotation de la planète (ou de l'étoile).


==== Von Kármán ====

La dynamo de Von Kármán est schématisée ci-contre. Elle vise à reproduire, au laboratoire, les phénomènes propres à une dynamo planétaire. Elle utilise un volume de métal liquide (du gallium ou du sodium, liquides à des températures modérées) placé entre deux roues à palettes, qui peuvent tourner dans le même sens, ou en sens opposés, créant des mouvements tourbillonnaires. Des bobines de Helmholtz créent un champ magnétique variable (elles peuvent être placées pour qu'il soit longitudinal ou transversal) auquel le métal liquide est exposé. Ainsi, des courants induits apparaissent dans le métal, qui créent à leur tour un champ magnétique. Cette expérience a permis de reproduire le caractère auto-entretenu de la dynamo, et, surtout, fournit des données qui permettent de valider et d'améliorer les modèles numériques.Une itération de cette expérience, nommée VKS (Von Kármán Sodium), fonctionne depuis 2006 au Centre CEA de Cadarache.


== Description et caractérisation du champ d'une planète ==
Le champ d'une planète est représenté, en première approximation, comme le champ généré par un dipôle magnétique placé au centre de la planète. Le dipôle est caractérisé par son moment dipolaire (sa valeur), et par son orientation, c'est-à-dire l'angle qu'il forme d'avec l'axe de rotation de la planète. Ces paramètres sont déterminés par problème inverse à partir de mesure de champ magnétique effectuée en surface et/ou en orbite. Ils évoluent avec le temps.
Pour obtenir un modèle plus fin, un quadrupôle magnétique, voir un octopôle, peuvent être ajoutés. Ils viennent corriger les différences entre les mesures et le champ dipôle qui a été déterminé.

Dipôle ou quadrupôle magnétique
		
		
		


=== Développement en harmoniques sphériques ===
La développement du champ magnétique en harmoniques sphériques consiste à décomposer le potentiel scalaire du champ autour d'un objet céleste (planète ou lune) en une somme de fonctions élémentaires, un peu à la manière d'une transformation de Fourier qui décompose une fonction périodique en une somme infinie (mais dénombrable) de sinusoïdes. Le potentiel scalaire V est tel que le champ magnétique 
  
    
      
        
          
            
              B
              →
            
          
        
      
    
    {\displaystyle {\vec {B}}}
   soit l'opposé de son gradient :

  
    
      
        
          
            
              B
              →
            
          
        
        =
        −
        
          
            ∇
            →
          
        
        V
      
    
    {\displaystyle {\vec {B}}=-{\overrightarrow {\nabla }}V}
  .Le potentiel magnétique n'est défini que dans un domaine simplement connexe en l'absence de courants électriques (le champ magnétique étant alors irrotationnel). Dans le cas qui nous intéresse, il est défini à l'extérieur de l'objet céleste.
Comme on s'intéresse à des objets célestes de forme sphéroïdale (en première approximation), il est naturel de travailler en coordonnées sphériques, donc d'écrire V comme une fonction des coordonnées sphériques r (distance au centre de l'objet), θ (colatitude, l'angle au pôle) et φ (longitude). On note a le rayon de l'objet étudié. V se décompose en deux termes pour le magnétisme dû aux sources internes (comme la dynamo active d'une planète) et aux sources externes. Sa composante interne s'écrit : 

  
    
      
        
          V
          
            
              i
              n
              t
            
          
        
        =
        a
        
          ∑
          
            n
            =
            1
          
          
            ∞
          
        
        
          
            (
            
              
                r
                a
              
            
            )
          
          
            1
            +
            n
          
        
        
          T
          
            n
          
          
            
              i
              n
              t
            
          
        
      
    
    {\displaystyle V_{\mathrm {int} }=a\sum _{n=1}^{\infty }\left({\frac {r}{a}}\right)^{1+n}T_{n}^{\mathrm {int} }}
  .Pour le potentiel dû aux sources externes, son écriture est similaire, mais en puissances croissantes de r : 

  
    
      
        
          V
          
            
              e
              x
              t
            
          
        
        =
        a
        
          ∑
          
            n
            =
            1
          
          
            ∞
          
        
        
          
            (
            
              
                a
                r
              
            
            )
          
          
            n
          
        
        
          T
          
            n
          
          
            
              e
              x
              t
            
          
        
      
    
    {\displaystyle V_{\mathrm {ext} }=a\sum _{n=1}^{\infty }\left({\frac {a}{r}}\right)^{n}T_{n}^{\mathrm {ext} }}
  .


== Autre formes de magnétisme ==


=== Magnétisme induit ===


=== Magnétisme crustal ===


== Planètes telluriques du système solaire ==


=== Mercure ===

La champ magnétique de Mercure a été mesuré pour la première fois en 1974 par la sonde Mariner 10. Son intensité est faible : environ 1,1 % de celle du champ Terrestre. Mais sa présence constituait déjà une surprise pour les scientifiques : Mercure étant petite, il était supposé que la planète serait totalement refroidie, qu'elle n'aurait plus aucun courant de convection et donc aucun champ magnétique. Les travaux plus récents confirment que le champ magnétique de Mercure est bien dû à un effet dynamo, quoique faible.


=== Vénus ===
Vénus ne possède aucun champ magnétique mesurable. La planète a pourtant une taille et une structure similaires à la Terre. La comparaison entre les deux planètes est donc un moyen de comprendre les mécanismes qui expliquent le champ magnétique planétaire. L'échange de chaleur convectif entre le noyau de Vénus et son manteau sont pratiquement terminés. Cela expliquerait à la fois l'absence de champ magnétique sur Vénus et son manque d'activité tectonique de 500 millions d'années.


=== Terre ===


==== Lune ====

Le champ magnétique actuel à la surface de la Lune est très faible (moins d'un centième du champ magnétique terrestre) et non dipolaire. Sa répartition géographique est dominée par les bassins d'impacts, les plus faibles (inférieurs à 0,2 nT) se trouvant dans les bassins les plus grands et les plus récents, Mare Orientale et Mare Imbrium, tandis que les champs les plus forts (supérieurs à 40 nT) sont mesurés au-dessus des surfaces diamétralement opposées à ces mêmes bassins. Le champ magnétique lunaire est entièrement dû à la magnétisation des roches crustales, et aujourd'hui la Lune ne possède pas de champ magnétique planétaire dipolaire. La magnétisation peut être due en partie à des champs magnétiques transitoires générés lors d'impacts importants, mais la majeure partie de cette magnétisation est héritée d'une époque où la Lune possédait un champ magnétique global, à l'instar de la Terre et d'autres planètes.
La présence d'un champ magnétique global peu après la formation de la Lune est attestée par l'aimantation rémanente de ses roches les plus anciennes. Les études paléomagnétiques montrent qu'une dynamo lunaire a fonctionné entre au moins 4,25 et 1,92 Ga et qu'une période de champ élevé (avec une intensité moyenne du champ d'environ 77 μT en surface) a perduré entre 3,85 et 3,56 Ga, suivie d'une baisse de l'intensité de surface jusqu'en dessous de 4 μT vers 3,19 Ga. La baisse d'un ordre de grandeur des paléo-intensités lunaires entre 3,56 et 3,19 Ga s'est suivie d'une période de champ bas (intensités du champ de surface de l'ordre de 5 μT) puis d'une seconde et dernière période de déclin entre 1,92 et 0,8 Ga, qui s'est terminée par l'arrêt de la dynamo lunaire, signe d'une cristallisation complète du noyau lunaire.


=== Mars ===

Mars ne possède pas aujourd'hui de champ magnétique global. Néanmoins, certaines zones de la croûte sont aimantées et créent un champ magnétique local, à la manière des anomalies magnétiques sur Terre. L'étude de ce magnétisme crustal, couplé à l'observation des traces de volcanisme a permis de déterminer que Mars a possédé, au début de son histoire, une dynamo magnétique, qui a été active pendant une durée probablement comprise entre 500 et 800 millions d'années. Ensuite, le flux de chaleur est devenu trop faible pour qu'une convection ait lieu dans le noyau ferreux.


=== Planètes naines ===


== Planètes géantes du système solaire ==


=== Jupiter ===

Jupiter possède le champ magnétique le plus puissant des planètes du Système solaire. Son moment magnétique est de 1,55 T m3, soit 20 000 fois celui de la Terre.
Contrairement aux autres planètes, l'existence d'un champ magnétique jovien a pu être attestée avant l'ère spatiale : en 1955, l'étude des émissions radio venant de cette planète a permis de mettre en évidence la présence d'aurores polaires, preuve indirecte de l'existence d'un champ magnétique global. L'effet Zeeman, c'est-à-dire le décalage des raies spectrales sous l'effet du champ magnétique, avait aussi pu être caractérisé depuis la Terre.
Des mesures directes du champ jovien ont été effectuées par quatre sondes des programmes Pioneer et Voyager. La sonde Juno, qui orbite Jupiter depuis 2016 (quand les quatre sondes précédentes s'en étaient seulement approchées), a fourni des informations beaucoup plus précises. Son orbite polaire lui permet en particulier de mesurer le champ magnétique dans l'axe de la planète, ce qui est une première concernant les planètes géantes.


==== Satellites galiléens ====
Les satellites galiléens possèdent chacun un faible champ magnétique, qui a pu être caractérisé grâce aux survols effectués par la sonde Galileo. Néanmoins, la question de savoir s'ils possèdent une dynamo active, et produisent donc leur champ magnétique de façon totalement autonome, ou si leur dipôle est induit par le champ magnétique jovien, n'est pas totalement tranchée. Concernant Io, il est très difficile de procéder à des mesures, car cette lune évolue si profondément dans le champ magnétique de Jupiter qu'il est difficile de distinguer son propre champ. Rien ne prouve la présence d'une dynamo active. Des travaux des années 2000 suggèrent que Io serait à l'équilibre thermique : la chaleur produite par les effets de marée dans son manteau serait suffisante pour que Io ne se refroidisse pas, il n'y aurait donc pas de courants de convection.
Europe et Callisto semblent n'avoir qu'un magnétisme induit. La meilleure explication actuelle repose sur leurs océans souterrains : l'eau liquide présente sur plusieurs kilomètres d'épaisseur serait le siège de courants induits suffisants pour expliquer leur réponse magnétique.
En revanche, dans le cas de Ganymède, la présence d'une dynamo active est probable, et son mécanisme principal serait la solidification du fer ou du sulfate de fer en périphérie de son noyau liquide.


=== Saturne ===
Le champ magnétique de Saturne est connu grâce aux sondes Pioneer 11, Voyager 1 et Voyager 2, et plus récemment Cassini. Par ailleurs, des aurores polaires ont été observées depuis la Terre. La dynamo de Saturne est probablement similaire, physiquement, à celle de Jupiter. Une caractéristique remarquable du champ de Saturne est qu'il est parfaitement aligné sur l'axe de rotation de la planète.


==== Titan ====
Du fait de sa taille, Titan a longtemps été considéré comme susceptible de posséder une dynamo active et d'avoir en conséquence son propre champ magnétique. Mais la mission Cassini-Huygens a montré que non, ce qui a profondément changé la conception que les scientifiques se font de cette lune : considérée auparavant comme totalement différenciée et avec un noyau ferreux (à l'image de Ganymède), elle est vue aujourd'hui comme partiellement différenciée avec un noyau peu dense, et sans aucune convection active. Il pourrait exister une réponse induite, comme dans le cas des lunes joviennes, mais elle serait très faible : le champ magnétique de Saturne étant parfaitement axisymétrique, sa variation vue par Titan au cours d'une orbite est minime.


=== Uranus et Neptune ===
Uranus et Neptune possèdent un champ magnétique très particulier, connu uniquement grâce aux mesures de la sonde Voyager 2, qui les a survolées respectivement en 1986 et 1989. Leur champ ne peut pas s'approcher simplement comme celui d'un dipôle. Il faut y ajouter un quadrupôle, dont la contribution en surface est aussi grande. De plus, leur axe magnétique n'est pas seulement incliné mais aussi décalé, c'est-à-dire qu'il ne passe pas par le centre de la planète,.
La nature inhabituelle, ni dipolaire, ni axisymétrique, du champ magnétique de ces deux planètes a pu être expliquée. Des travaux de simulation numérique ont montré qu'une telle configuration pouvait, dans des conditions bien précises, être retrouvée avec des modèles prenant en compte un effet dynamo limité à une zone assez petite autour d'un noyau fluide stable.


== Astéroïdes ==
Des corps beaucoup plus petits que les planètes ou les lunes ont eu, au début de l'histoire du système solaire, un champ magnétique auto-entretenu.  Des astéroïdes différentiés possédant un noyau ferreux ont connu une phase de refroidissement pendant laquelle leur noyau était partiellement liquide et sujet à des courants de convection, certains sont passés par une phase où les conditions étaient réunies pour qu'une dynamo auto-entretenue soit possible. Cette période a été très courte, comparativement à des planètes comme la Terre : les effets d'échelle sont tels qu'un corps de petite taille se refroidit bien plus rapidement. À l'échelle d'un astéroïde, les modèles suggèrent que la convection thermique est peu efficace pour actionner une dynamo et que la convection de composition est plus efficace. Elle se produit lorsque le noyau a commencé à se solidifier, du fait de la solidification du soufre, du nickel et du fer à différentes températures. Un modèle numérique concernant un astéroïde d'un diamètre de 400 km a été publié. Il montre qu'une dynamo active a pu être possible pendant cinq millions d'années, commençant 5 millions d'années après l'événement d'accrétion formant le corps considéré. Ces périodes varient selon la composition de l'objet. 
Le champ magnétique des astéroïdes nous est connu par l'aimantation rémanente des météorites qui en proviennent. L'existence d'une aimantation rémanente dans des météorites de différents types, différenciées ou non, est connue depuis les années 1960, mais son décryptage est resté longtemps difficile. Les techniques de désaimantation contrôlée s'étant depuis considérablement améliorées, des résultats concluants se sont accumulés depuis le début des années 2000. De nombreuses météorites sont porteuses d'une aimantation thermorémanente dont on peut montrer qu'elle a été acquise quand la météorite était encore au sein de son corps parent. Ces corps parents possédaient donc un champ magnétique, et d'autres astéroïdes sans doute aussi.


=== Météorites différenciées ===
Majoritairement constituées d'un métal ferromagnétique, les météorites de fer sembleraient à première vue propices à la rétention d'une aimantation thermorémanente, qui témoignerait d'un champ magnétique créé par effet dynamo dans le noyau de leurs corps parents. L'aimantation des phases principales (kamacite et taénite) est en fait trop fragile (trop sensible à toutes les perturbations subies par la météorite), d'où le fait que les météorites de fer ont fait l'objet d'assez peu d'études paléomagnétiques. Il existe cependant de petites zones troubles (cloudy zones) contenant de minuscules grains de tétrataénite, à l'aimantation beaucoup plus fiable (forte coercivité). Mais les météorites de fer posent un autre problème : si elles proviennent du noyau d'un astéroïde différencié, leur température n'a dû descendre en dessous de la température de Curie qu'après la cristallisation du noyau et donc longtemps après l'arrêt de la dynamo. De fait, les zones troubles de Tazewell, une météorite du groupe IIICD, n'ont enregistré aucun champ magnétique. D'autres types de météorites de fer sont plus prometteurs, notamment le groupe IIE (interprété comme provenant de poches de métal relativement superficielles) et le groupe IVA (refroidi particulièrement vite).
Les pallasites (des météorites mixtes métal-silicate), au moins celles du groupe principal, ont enregistré dans leur olivine et dans leurs grains de tétrataénite, un champ magnétique de longue durée (jusqu'à des centaines de millions d'années après l'accrétion,), attribuable à une dynamo auto-entretenue liée à la convection solutale engendrée par la cristallisation progressive du noyau de leur corps parent.
L'aimantation rémanente de l'eucrite ALH A81001, une achondrite basaltique formée il y a 3,69 milliards d'années, sans doute à la surface de l'astéroïde (4) Vesta, témoigne d'un champ magnétique d'au moins 2 µT à cette époque, sans doute dû à l'activité d'une dynamo dans le noyau métallique de Vesta.
Le corps parent des angrites (des achondrites magmatiques) possédait un champ magnétique d'au moins 10 µT entre −4 564 et −4 558 Ma, disparu avant −3 700 Ma,.


=== Météorites indifférenciées ===
L'une des composantes de l'aimantation rémanente de la météorite de Murchison, une chondrite carbonée du groupe CM2, a été acquise pendant ou après la formation de son corps parent, en présence d'un champ magnétique d'au moins 0,2 à 2 µT. La météorite d'Allende, une chondrite carbonée du groupe CV3, porte une aimantation thermorémanente acquise dans son corps parent à la fin d'un épisode de métamorphisme, dans un champ magnétique faible (< 8 µT), sans doute moins de 40 Ma après la formation des CAI. Les chondres de la météorite de Vigarano, une autre CV, possèdent une aimantation thermorémanente acquise probablement lors de la rupture du corps parent 9 Ma environ après la formation des CAI, quand la dynamo de cet astéroïde était encore active.
La météorite de Farmington, une chondrite ordinaire du groupe L5, possède une aimantation thermorémanente acquise dans son corps parent il y a moins de 520 Ma. Les chondres de la météorite de Bjurböle, une chondrite ordinaire du groupe L/LL4, portent une aimantation thermorémanente qui témoigne que la dynamo du corps parent était encore active 80 à 140 Ma après le pic de son métamorphisme. La météorite de Portales Valley, une chondrite ordinaire du groupe H6, a de même enregistré un champ magnétique pendant des dizaines à des centaines d'années, environ 100 Ma après la formation du Système solaire.Il semble ainsi que l'intérieur des corps parents des chondrites devait être différencié, au moins partiellement. Dans ce cadre, les chondrites proviendraient des couches externes de ces astéroïdes, non différenciées.


== Tableau comparatif ==


== Exoplanètes ==
Des preuves d'interaction magnétique entre une exoplanète et son étoile ont été obtenues pour de nombreux Jupiter chauds. En 2019, l'intensité du champ magnétique à la surface de quatre de ces Jupiter chauds a pu être mesurée : entre 20 et 120 G. C'est dix à cent fois plus que ce que prédisent les modèles de dynamo pour les planètes ayant une période de rotation de 2 à 4 jours, mais en accord avec les modèles fondés sur le flux de chaleur à l'intérieur des planètes géantes. À titre de comparaison, cette intensité est de 4,3 G pour Jupiter et de 0,5 G pour la Terre.


== Notes et références ==


== Voir aussi ==
Magnétosphère
Champ magnétique stellaire Portail de la physique   Portail de l’astronomie   Portail des planètes mineures et comètes   Portail des exoplanètes