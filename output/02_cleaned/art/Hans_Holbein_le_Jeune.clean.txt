hans holbein le jeune, né à augsbourg, en bavière, vers 1497 et mort à londres en 1543, entre le 8 octobre et le 29 novembre, est un peintre et graveur de la renaissance allemande, d'abord sujet du saint-empire romain germanique, puis citoyen de bâle (ancienne confédération suisse). une de ses œuvres les plus connues est les ambassadeurs, peinte en 1533 et représentant deux envoyés, l'un de la noblesse, l'autre du clergé. il excelle particulièrement dans les portraits, comme celui du roi d'angleterre henri viii. == biographie ==
fils du peintre hans holbein l'ancien, il est le frère cadet du peintre ambrosius holbein (c.1494 - c.1519), avec lequel il étudie dans l'atelier paternel qui était alors un des plus fameux et recherché de la ville. hans et son frère ambrosius profitèrent aussi sans doute de l'enseignement de leur oncle, hans burgkmair, l'un des plus grands peintres allemands de l'époque.
en 1515, il se rend à bâle, haut lieu de l'humanisme. de 1516 à 1526. il réalise des portraits, pour la haute bourgeoisie commerçante, en particulier celui des époux meyer, jakob meyer étant alors bourgmestre de la ville. en 1517, à lucerne, l'administration communale recommande de petits travaux de décoration et il réalise des fresques, aujourd'hui détruites, pour la demeure du bourgmestre jacob von hertenstein, pour lesquelles il fit probablement appel à son père. les dessins préparatoires montrent des innovations qui évoquent un voyage à milan avec son oncle hans burgkmair pendant l'année 1518.
en 1519, de retour à bâle, il est conféré maître par la guilde des peintres et signe le portrait de bonifacius amerbach, humaniste, hommes de lettres et juriste. amerbach est un vieil ami d'érasme et lui présente holbein quatre ans plus tard. la même année, il épouse elsbeth, une riche veuve. en 1520, il obtient la citoyenneté bâloise et en 1521 commence les fresques aujourd'hui disparues de la salle du grand conseil. c'est aussi l'année de la naissance de son fils philipp.
en 1523, il rencontre érasme de rotterdam qui vit à bâle depuis 1521 et fait de lui deux portraits, qui deviennent plus tard son laisser-passer pour l'angleterre où érasme les fait envoyer à des amis anglais.
lors d'un voyage en france en 1524, il découvre léonard de vinci. il y apprend sa fameuse technique « des trois crayons », consistant à exécuter les portraits à l'encre noire, à la sanguine et à la craie blanche.
durant cette période, il accomplit également de nombreux voyages en italie, à rome et florence. influencé par matthias grünewald, son style s'ouvre aux nouvelles conceptions de la renaissance italienne. il travaille également à des compositions religieuses, décorations murales, cartons de vitraux et gravures. en 1526, fuyant la réforme, il part pour londres, recommandé par érasme à thomas more. il revient à bâle en 1528, où il s'achète une maison, après avoir vécu en angleterre dans une atmosphère de liberté intellectuelle et spirituelle qui va lui manquer à bâle. la ville est alors en proie au fanatisme et à l'intolérance religieuse qui font fuir érasme réfugié à fribourg. il est donc de retour à londres en 1533. mais là aussi thomas more est tombé en disgrâce et la liberté d'esprit n'est plus au rendez-vous. ses commanditaires ne sont plus les humanistes mais les riches marchands qui veulent être représentés avec tous les attributs de leur pouvoir. cette époque constitue l'apogée de sa carrière. il exécute le projet d'un arc de triomphe pour l'entrée d'anne boleyn à londres et peint le tableau les ambassadeurs en 1533. ce dernier est particulier. en effet, une partie de cette œuvre est réalisée selon le procédé de l'anamorphose. ainsi, si le regard se positionne par rapport à la tranche droite du tableau, on voit apparaître un crâne humain au milieu des deux personnages. la présence de ce crâne fait de ce tableau un memento mori, qui rappelle à l'humilité. les deux personnages représentés sont invités à se souvenir qu'ils sont mortels comme tout un chacun.
en 1536, nommé peintre-valet de chambre d'henri viii, il devient en peu de temps le peintre officiel de la cour d'angleterre. entre 1538 et 1539, il voyage en europe afin de faire le portrait des princesses candidates au mariage avec henri viii après la mort de jeanne seymour. en 1540, c'est anne de clèves que le roi épouse.
en 1543, entre le 7 octobre et le 29 novembre, en pleine gloire, il meurt de maladie à l'âge de 46 ans environ. karel van mander déclara au début du xviie siècle qu'holbein mourut de la peste mais cette assertion est à prendre avec précaution: elle est remise en question par plusieurs historiens. wilson, par exemple, doute de cette histoire du fait de la présence des amis d'holbein à son chevet au moment de son trépas et peter claussen suggère qu'il mourut plutôt des suites d'une infection. son testament nous apprend l'existence de deux fils naturels, qu'il dote généreusement.
recherchant derrière les apparences les expressions marquantes des visages, il cherche à réunir les traditions gothiques et les nouvelles tendances humanistes. == réception == selon anna dostoïevskaïa, qui fait un rapport circonstancié de l'incident, et qui craint une nouvelle crise d'épilepsie à cette occasion, fiodor dostoïevski, grand admirateur d'holbein, est fort secoué lorsqu'il voit à bâle, en juin 1867, le tableau le corps du christ mort dans la tombe; selon lui, « ce tableau peut faire perdre la foi. » le tableau l'a tellement troublé qu'il en fait une brève description dans l'idiot. == œuvres == === à bâle ===
le musée d'art de la ville de bâle possède la plus importante collection au monde d'œuvres de la famille holbein. 1516: portraits de jacob meyer et de dorothée kannengiesser, détrempe sur panneau, 38,5 × 31 cm chaque, kunstmuseum (bâle)
1519: portrait de bonifacius amerbach, détrempe sur panneau, 48 × 35 cm, kunsthistorisches museum de vienne
1521-22: le corps du christ mort dans la tombe, détrempe sur panneau, 30,5 × 200 cm, kunstmuseum (bâle)
1521-1522 retable oberried, détrempe sur panneau: adoration des bergers et adoration des mages, 231 × 110 cm, dôme de fribourg
1522: la madone de soleure huile sur bois de tilleul, 143,3 × 104 cm, musée des beaux-arts, soleure
1523 :
portrait d'érasme de rotterdam, huile sur bois, 42 × 32 cm, musée du louvre, paris
portrait d'érasme de rotterdam, détrempe sur papier, marouflé sur bois de pin, 37 × 30 cm, kunstmuseum (bâle)
1526: laïs de corinthe, huile sur bois de tilleul, 37 × 27 cm, kunstmuseum (bâle)
1526-1528: dame à l'écureuil et à l'étourneau, huile sur chêne, 56 × 39 cm, national gallery, londres
1526 repris en 1529 :
la vierge et l'enfant avec la famille du bourgmestre meyer, retable, huile sur bois, 146,5 × 102 cm, darmstadt, schlossmuseum
1527 :
william warham (1457-1532), archevêque de canterbury en 1504, bois, 82 × 66 cm, musée du louvre
sir thomas more, huile sur panneau, 75 × 60 cm, frick collection, new york
anne cresacre, bru de thomas more, pierre noire et pastel sur papier, 37,9 × 26,9 cm, royal library, château de windsor.
nicholas kratzer, huile sur toile, 81,9 × 64,8 cm, musée du louvre
portrait de sir bryan tuke, huile sur panneau, 49 × 38 cm, national gallery of art, washington
1528 :
la femme du peintre avec ses deux aînés, détrempe, 77 × 64 cm, kunstmuseum (bâle)
portrait de thomas godsalve et de son fils john godsalve, détrempe sur panneau, 35 × 36 cm, gemäldegalerie alte meister, dresde
1532: portrait de georg gisze, huile sur bois, 96,3 × 85,7 cm, gemäldegalerie (berlin)
1532: portrait d'un membre de la famille wedigh, probablement hermann wedight, huile sur bois (2 planches de chêne de baltique), 42,2 × 32,4 cm, metropolitan museum of art, new york.
1532-1533: thomas cromwell, huile sur panneau, 78 × 64 cm, frick collection, new york
1533 :
les ambassadeurs, huile sur panneau, 207 × 209 cm, londres, national gallery
portrait de dirk tybis, détrempe sur panneau, 48 × 35 cm, kunsthistorisches museum de vienne
portrait de robert cheseman, 1533, détrempe sur panneau, 59 × 63 cm, mauritshuis, la haye
1534-1535: portrait d'homme au luth, tempera sur bois, 43 × 43 cm, gemäldegalerie (berlin)
1536: portrait de sir richard southwell, huile sur bois, 47,5 × 38 cm, florence, galerie des offices. === à la cour d'angleterre ===
1536: portrait de jane seymour, reine d'angleterre, panneau de chêne, 65 × 51 cm, kunsthistorisches museum de vienne
1536-1537 :
portrait d'henri viii, détrempe sur panneau, 26 × 19 cm, collection thyssen
portrait d'henri viii, huile sur bois, 237,7 × 134,6 cm, walker art gallery, liverpool
portrait de jeanne seymour, détrempe sur panneau, 26 × 19 cm, mauritshuis, la haye. serait une réplique de l'original conservé à vienne par l'artiste et l'atelier.
1538 :
portrait de christine de danemark, détrempe sur panneau, 178 × 81 cm, national gallery (londres)
portrait de jean bugenhagen, chantilly, musée condé
sir henry wyatt, musée du louvre
portrait d'edouard vi, prince de galles à deux ans, détrempe sur panneau, 57 × 44 cm, national gallery of art, washington
1539 :
portrait d'anne de clèves, reine d'angleterre, quatrième épouse d'henri viii, 1539, vélin sur toile, 65 × 48 cm, musée du louvre
thomas howard, huile sur panneau, 81 × 61 cm, royal collection, château de windsor
1539-1540: henri viii, détrempe sur panneau, 88 × 75 cm, galerie nationale d'art ancien (rome)
vers 1540: portrait de jane pemberton small
1541: portrait d'un jeune marchand, panneau de chêne, 46 × 35 cm, kunsthistorisches museum de vienne
1542: autoportrait, pastels colorés, 32 × 26 cm, musée des offices, florence
1543 :
portrait d'antoine de lorraine, berlin, gemäldegalerie
portrait d'edouard, prince de galles, détrempe sur panneau, diam. 32,4 cm, metropolitan museum of art, new york
docteur john chambers, médecin d'henri viii, panneau de chêne, 58 × 40 cm, kunsthistorisches museum de vienne === gravures ===
en 1526, il réalise une série de 41 gravures sur bois: « danse macabre ». elles seront publiées sans texte en 1530, puis en 1538, dans un recueil intitulé « simulacres et historiées faces de la mort »; en 1545 les gravures, primitivement au nombre de 41, furent portées à 53 et accompagnées de sentences latines et de quatrains moraux français. il ne s’agit pas de farandoles où la mort entraîne ses victimes vers leur fin et les scènes ne se passent pas non plus dans les cimetières, la mort fait irruption dans la vie quotidienne, elle interrompt les activités de chacun, qu’il s’agisse du travail du négociant, de l’activité du juge, du médecin ou encore du chevalier. la mort surprend les hommes dans leurs occupations ou dans les plaisirs qu’offre la vie; elle ne fait aucune distinction d’ordre ou de classe. cependant, toujours agressive et moqueuse, « la mort » d'holbein prend les allures d’un justicier, l’œuvre de l’artiste a un côté subversif dans la mesure où il dénonce les abus du pouvoir, les autorités religieuses qui profitent de leur statut et la puissance des plus riches. certes, il montre que la mort touche tout le monde mais avec ironie et férocité il ridiculise les puissants (dans le domaine religieux et politique) en dénonçant leurs travers ou leurs manquements au rang qu’ils doivent tenir ou aux serments prononcés. gray, musée baron-martin: portrait de femme, gravure, 40 × 28 cm. === dessins ===
christ au repos, 1519, dessin à la plume et au pinceau sur papier brun, kupferstichkabinett, berlin
étude de mouvement d'un corps féminin, 1535, dessin à la plume et au pinceau, kunstmuseum (bâle)
portrait de jeune homme, pierre noire et sanguine, 30 × 19 cm, collection ian woodner, new york === galerie === == notes et références == === notes === === références === == annexes == === bibliographie ===
ackroyd, peter. the life of thomas more. londres: chatto & windus, 1998. (isbn 1-85619-711-5).
erna auerbach. tudor artists: a study of painters in the royal service and of portraiture on illuminated documents from the accession of henry viii to the death of elizabeth i. londres: athlone press, 1954. oclc 1293216.
oskar bätschmann et pascal griener, hans holbein, gallimard, 1997, (isbn 2070115542)
beyer, andreas. "the london interlude: 15261528." dans hans holbein the younger: the basel years, 15151532, müller, et al., 6671. munich: prestel, 2006. (isbn 3-7913-3580-4).
borchert, till-holger. "hans holbein and the literary art criticism of the german romantics." dans hans holbein: paintings, prints, and reception, édité par mark roskill & john oliver hand, 187209. washington: national gallery of art, 2001. (isbn 0-300-09044-7).
brooke, xanthe, and david crombie. henry viii revealed: holbein's portrait and its legacy. londres: paul holberton, 2003. (isbn 1-903470-09-9).
buck, stephanie. hans holbein, cologne: könemann, 1999, (isbn 3-8290-2583-1).
calderwood, mark. "the holbein codes: an analysis of hans holbein’s the ambassadors ". newcastle (au): university of newcastle, 2005. récupéré le 29 novembre 2008.
claussen, peter. "holbein's career between city and court." dans hans holbein the younger: the basel years, 15151532, müller et al., 4657. munich: prestel, 2006. (isbn 3-7913-3580-4).
foister, susan. holbein in england. londres: tate: 2006. (isbn 1-85437-645-4).
foister, susan; ashok roy; & martyn wyld. making & meaning: holbein's ambassadors. londres: national gallery publications, 1997. (isbn 1-85709-173-6).
ganz, paul. the paintings of hans holbein: first complete edition. londres: phaidon, 1956. oclc 2105129.
gaunt, william. court painting in england from tudor to victorian times. londres: constable, 1980. (isbn 0-09-461870-4).
hearn, karen. dynasties: painting in tudor and jacobean england, 15301630. londres: tate publishing, 1995. (isbn 1-85437-157-6).
ives, eric. the life and death of anne boleyn. oxford: blackwell, 2005. (isbn 978-1-4051-3463-7).
king, david j. "who was holbein's lady with a squirrel and a starling?". apollo 159, 507, may 2004: 16575. rpt. sur bnet.com. récupéré le 27 novembre 2008.
landau, david, & peter parshall, the renaissance print, new haven (ct): yale, 1996, (isbn 0-300-06883-2).
(de) hanspeter lanz, « "hans von zürch goldschmidt" - ein verschollenes porträt von hans holbein d. j. », revue suisse d'art et d'archéologie, vol. 73, nos 1/2, 2016, p. 87-91 (issn 0044-3476).
michael, erika. "the legacy of holbein's gedankenreichtum." dans hans holbein: paintings, prints, and reception, édité par mark roskill et john oliver hand, 22746. washington: national gallery of art, 2001. (isbn 0-300-09044-7).
müller, christian; stephan kemperdick; maryan w. ainsworth; et al.. hans holbein the younger: the basel years, 15151532. munich: prestel, 2006. (isbn 3-7913-3580-4).
north, john. the ambassadors' secret: holbein and the world of the renaissance. londres: phoenix, 2004. (isbn 1-84212-661-x).
parker, k. t. the drawings of hans holbein at windsor castle. oxford: phaidon, 1945. oclc 822974.
reynolds, graham. english portrait miniatures. cambridge: cambridge university press, 1988. (isbn 0-521-33920-0).
roberts, jane, holbein and the court of henry viii, national gallery of scotland, (1993). (isbn 0-903598-33-7).
roskill, mark, & john oliver hand (eds). hans holbein: paintings, prints, and reception. washington: national gallery of art, 2001. (isbn 0-300-09044-7).
rowlands, john. holbein: the paintings of hans holbein the younger. boston: david r. godine, 1985. (isbn 0-87923-578-0).
sander, jochen. "the artistic development of hans holbein the younger as panel painter during his basel years." dans hans holbein the younger: the basel years, 15151532, müller et al., 1419. munich: prestel, 2006. (isbn 3-7913-3580-4).
scarisbrick, j. j. henry viii. londres: penguin, 1968. (isbn 0-14-021318-x).
schofield, john. the rise & fall of thomas cromwell. stroud (uk): the history press, 2008. (isbn 978-0-7524-4604-2).
starkey, david. six wives: the queens of henry viii. londres: vintage, 2004. (isbn 0-09-943724-4).
strong, roy. holbein: the complete paintings. londres: granada, 1980. (isbn 0-586-05144-9).
waterhouse, ellis. painting in britain, 15301790. londres: penguin, 1978. (isbn 0-14-056101-3).
wilson, derek. hans holbein: portrait of an unknown man. londres: pimlico, revised edition, 2006. (isbn 978-1-84413-918-7).
zwingenberger, jeanette. the shadow of death in the work of hans holbein the younger. londres: parkstone press, 1999. (isbn 1-85995-492-8).
mauro zanchi, "holbein", art e dossier, giunti, florence 2013. (isbn 9788809782501) ==== œuvres de fiction ====
harry bellet, les aventures extravagantes de jean jambecreuse, actes sud (tomes 1, 2 et 3), romans historiques inspirés de la vie du peintre hans holbein.
michel winter, le secret des ambassadeurs, pol'art historique, autoédition amazon. === articles connexes ===
iconoclasme protestant suisse et allemand (surtout en 1520-1530) === liens externes === ressources relatives aux beaux-arts: agorha bridgeman art library galerie nationale de finlande j. paul getty museum musée des beaux-arts du canada national gallery of victoria royal academy of arts sikart tate (en) art uk (en) auckland art gallery (en) bénézit (en) british museum (en + de) collection de peintures de l'état de bavière (en) cooperhewitt, smithsonian design museum (da + en) kunstindeks danmark (en) musée d'art nelson-atkins (de + en) musée städel (en + es) musée thyssen-bornemisza (en) museum of modern art (en) mutualart (en) national gallery of art (en) national portrait gallery (en + sv) nationalmuseum (en + nl) rkdartists (de + en + la) sandrart.net (en) smithsonian american art museum (en) te papa tongarewa (en) union list of artist names ressources relatives à la musique: (en) musicbrainz (en + de) répertoire international des sources musicales ressources relatives à la recherche: biodiversity heritage library persée ressource relative à la littérature: (en) internet speculative fiction database portail de la peinture portail de la gravure et de l'estampe portail de la renaissance portail du saint-empire romain germanique portail de la suisse du nord-ouest