# -*- UTF-8 -*-

"""
- author: Floriane Chiffoleau
- date: July 2022
- description: Retrieving the content of Wikipedia pages
- input: URL/Title of Wikipedia pages
- output: Whole content in TEXT form of the Wikipedia page
- usage :
    ======
    python name_of_this_script.py arg1
    arg1: URL of the wikipedia page or title of the page
"""

import sys
from typing import Tuple
import wikipedia
import requests
import os
from urllib.parse import unquote


os.makedirs("output", exist_ok=True)
#Create the output directory when it does not exist

"""
#Method 1

def get_article(): # Tuple[str, str]:
	req = requests.get(sys.argv[1])
	#Retrieve the URL of the article that we want (stored in a bash script in order to have all of our pages at once)
	article_url = req.url
	article_id = article_url.split("/")[-1]
	#Retrieve the name of the page at the end of the URL
	return article_url, article_id

def write_article(art_id: str, art_url: str, sentences: str):
	with open(f"output/{art_id}.txt", "w") as f:
		f.write(art_url+"\n\n"+sentences)
		#Write the Wikipedia page with the URL, two linebreaks and then the requested content (the whole page here)


wikipedia.set_lang("fr")

art_url, art_id = get_article()
write_article(art_id, art_url, ".".join(wikipedia.page(unquote(art_id)).content.split(".")))
print(f"Writing {article_url}")
#This requests that, for the article to be written, the script looks for the URL, retrieve the "title" of the page 
#and from that, write its content


"""
#Method 2

def write_article(sentences: str):
	with open(f"output/" + sys.argv[1] + ".txt", "w") as f:
		f.write(sentences)
		#Write the Wikipedia page (the whole page )

wikipedia.set_lang("fr")

write_article(".".join(wikipedia.page(sys.argv[1]).content.split(".")))
print(f"Writing article for " + sys.argv[1])
#This requests that, for the article to be written, the script retrieve the "title" of the page and from that, write its content