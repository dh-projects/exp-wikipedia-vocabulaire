# -*- UTF-8 -*-

"""
- author: Floriane Chiffoleau
- date: July 2022
- description: Creating frequency lists for words and characters
- input: TXT file
- output: CSV files
- usage :
    ======
    python name_of_this_script.py arg1 arg2 arg3
    arg1: TXT file with the content to analyze
    arg2: CSV file with the list of words and their occurences
    arg3: CSV file with the list of characters and their occurences
"""

import os
import re
import sys
from collections import Counter

def delete_punctuation(text):
    """ Deleting punctuation marks from the text
    
    :param text: Text to clean
    :type text: str
    :returns: Texte without punctuation
    :rtype: str
    """
    punctuation = "!:;\",?'’.°"
    for marker in punctuation:
        text = text.replace(marker, " ")
    return text

with open(sys.argv[1], 'r') as xml_file:
    text = xml_file.read()
    text = text.lower()
    text = delete_punctuation(text)

    #Method 1: Working with the words
    w = text.split()
    words = Counter(w)
    sortedDict = sorted(words.items(), key=lambda x: x[1], reverse=True)
    #Sort the dictionnary by the values (number of occurences)
    words = str(sortedDict)
    words = re.sub(r"\), \('", "\n", words)
    words = re.sub("', ", "\t", words)
    words = re.sub(r"\[\('|\)\]", "", words)
    
    #Create elements for a CSV file
    words = re.sub(r"\n", '"\n"', words)
    words = re.sub(r"^", '"Word", "Number of occurrences"\n"', words)
    words = re.sub(r"$", '"', words)
    words = re.sub(r"\t", '", "', words)


    #Method 1: Working with the characters
    c = [i for i in text]
    characters = Counter(c)
    sortedDict = sorted(characters.items())
    #Sort the dictionnary by the keys (the character)
    characters = str(sortedDict)
    characters = re.sub(r"\), \('", "\n", characters)
    characters = re.sub("', ", "\t", characters)
    characters = re.sub(r"\[\('|\)\]", "", characters)
    
    #Create elements for a CSV file
    characters = re.sub(r"\n", '"\n"', characters)
    characters = re.sub(r"^", '"Character", "Number of occurrences"\n"', characters)
    characters = re.sub(r"$", '"', characters)
    characters = re.sub(r"\t", '", "', characters)

with open(sys.argv[2],"w", encoding='UTF-8') as file_out:
    print("writing to "+sys.argv[2])
    file_out.write(words)
    #Write the table with the words from the corpus

with open(sys.argv[3],"w", encoding='UTF-8') as file_out:
    print("writing to "+sys.argv[3])
    file_out.write(characters)
    #Write the table with the characters from the corpus
