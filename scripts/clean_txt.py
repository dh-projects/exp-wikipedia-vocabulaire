# -*- UTF-8 -*-

"""
- author: Alix Chagué
- date: July 2022
- description: Clean content of txt file extracted from Wikipedia
- input: txt files
- output: txt files
- usage :
    ======
    python name_of_this_script.py arg1 arg2
    arg1: folder containing txt files
"""

import argparse
import os
import re

from tqdm import tqdm


def get_list_of_files(path_folder):
    """List usable files from input folder"""
    abs_path_folder = os.path.abspath(path_folder)
    file_paths = []
    if os.path.isdir(abs_path_folder):
        file_paths = [os.path.join(abs_path_folder, f) for f in os.listdir(abs_path_folder)]
    else:
        print("Failed to load input. Are you sure the path to ",
        "the directory is correct:\n",
        "\t", abs_path_folder)
    file_paths = [file for file in file_paths if file.endswith(".txt") and not file.endswith(".clean.txt")]
    return file_paths

def load_text_from_file(path):
    """Load content of a file if it exists"""
    text = False
    if os.path.exists(path):
        with open(path, "r", encoding="utf8") as fh:
            text = fh.read()
    return text 

def save_new_text(clean_text, path):
    """Save a text in a file"""
    with open(path, "w+", encoding="utf8") as fh:
        fh.write(clean_text)

def create_output_basename(path):
    """Create path for output files"""
    basename = ".".join(os.path.basename(path).split(".")[:-1])
    return os.path.join(os.path.dirname(path), basename)

def text_cleaner(orig_text, lower=True, rm_appendix=True, rm_chars=True):
    """Clean the text by removing undesired elements"""
    # 1. lower
    if lower:
        text = orig_text.lower()
    # 2. remove extra spaces:
    text = re.sub(r"\s{2,}", " ", text, 0, re.MULTILINE)
    text = re.sub(r"\s{2,}", " ", text, 0, re.MULTILINE) # needed twice
    # this is to avoid weird segmentation
    text = re.sub(r" ([\(\[{]) ", r" \1", text, 0, re.MULTILINE)
    text = re.sub(r" ([)}\]]) ", r"\1 ", text, 0, re.MULTILINE)
    text = re.sub(r" ([\?!:;\.,]) ", r"\1 ", text, 0, re.MULTILINE)
    # 3. remove appendix
    if rm_appendix:
        line_by_line = []
        for line in text.split("\n"):
            if line.startswith("="):
                line_by_line.append(f"""#&-##{line.replace("===", "==").replace("===", "==")}""")
            else:
                line_by_line.append(line)
        marked_text = "\n".join(line_by_line)
        sections = marked_text.split("#&-##")

        rejected_titles = ["== liens externes", "== bibliographie", "== notes et références",
                        "== notes ==", "== annexes ==", "== galerie ==", "== références ==",  
                        "== articles connexes", "== article connexe", "== bases de données et dictionnaires"]
        kept_sections = [sec for sec in sections if not sec.startswith("== voir aussi")]
        for rejected_title in rejected_titles:
            kept_sections = [sec for sec in kept_sections if not sec.startswith(rejected_title)]
        text = "\n".join(kept_sections)
    # 4. remove rejected characters
    if rm_chars:
        accepted_characters = "".join(["""àâäéèêëîïôöûüùÿçæœ….,;:!?¿¡*-—_¬·'’`´`„‚“”"«»&#()[]\{\}/\\§ß@£¥$""",
                                "0123456789≤≥<>×÷=+°%≠±", "abcdefghijklmnopqrstuvwxyz \n\t"])
        text = "".join([c for c in text if c in accepted_characters])
    # 5. remove empty lines
    text = "\n".join([line for line in text.split("\n") if len(line) > 0])
    return text

def main(input_path):
    file_paths = get_list_of_files(input_path)

    for file in tqdm(file_paths):
        output_name = create_output_basename(file)
        text = load_text_from_file(file)
        if text:
            clean_text = text_cleaner(text)
            save_new_text(clean_text, f"{output_name}.clean.txt")
        else:
            print("no text")


parser = argparse.ArgumentParser()
parser.add_argument("--test", help="execute on test files", action="store_true")
parser.add_argument("-i", "--input", nargs="?", help="path to input files")
args = parser.parse_args()

if args.test:
    main("test/")
else:
    if args.input:
        main(args.input)
    else:
        print("You need to provide a path to some source files (-i/--input PATH)")

