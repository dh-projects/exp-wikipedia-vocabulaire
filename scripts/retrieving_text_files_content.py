
# -*- UTF-8 -*-

"""
- author: Floriane Chiffoleau
- date: July 2022
- description: Retrieving the content of the text files
- input : TXT files
- output: TXT file
- usage :
    ======
    python name_of_this_script.py arg1 arg2
    arg1: folder of the TXT files
    arg2: name of the output file in a TXT format
"""

import os
import sys
from bs4 import BeautifulSoup

for root, dirs, files in os.walk(sys.argv[1]):
    for filename in files:
        with open(sys.argv[1] + filename, 'r') as xml_file:
            print("Reading from " + filename)

            processed_text_as_list = []
            text = xml_file.read()

        with open(sys.argv[2], "a") as file_out:
            print("Writing in " + sys.argv[2])
            #Warning: the output file is in a "add" mode, which mean that it will add text, file after file
            file_out.write(text)
