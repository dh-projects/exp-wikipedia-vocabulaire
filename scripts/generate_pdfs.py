# -*- UTF-8 -*-

"""
- author: Alix Chagué
- date: July 2022
- description: Create PDF files based on txt files
- input: txt files
- output: pdf files
- usage :
    ======
    python name_of_this_script.py arg1 arg2
    arg1: folder containing txt files
"""

import argparse
import os
from textwrap import wrap

from fpdf import FPDF
import numpy as np
from tqdm import tqdm

def define_font(pdf_obj, font):
    """Try to set GNUFreeMono as font, otherwise use Arial"""
    # TODO: make the choice of font more explicit.
    # ../fonts/freefont-20120503/FreeMono.ttf
    # ../fonts/freefont-20120503/FreeSans.ttf
    # ../fonts/freefont-20120503/FreeSerif.ttf

    fonts = {"mono":"FreeMono", "sans": "FreeSans", "serif": "FreeSerif"}

    if font in fonts.keys():
        font = {"name":f"GNU{fonts[font]}", "path": os.path.abspath(os.path.join("fonts","freefont-20120503",f"{fonts[font]}.ttf"))}
    else:
        print(f"PDF will use GNUFreeMono font. Make sure 'FreeMono.ttf'",
              f"""is in {os.path.abspath(os.path.join("fonts","freefont-20120503","FreeMono.ttf"))}""")
        font = {"name":"GNUFreeMono", "path": os.path.abspath(os.path.join("fonts","freefont-20120503","FreeMono.ttf"))}
    chosen_font = font["name"]
    try:
        pdf_obj.add_font(font["name"], "", font["path"], uni=True)
        chosen_font = font["name"]
    except Exception as e:
        print("Could not load TTF file for Font definition.", 
        "Setting 'Arial' as font, but this will cause issue if encoding is UTF8.", sep="\n")
        chosen_font = "Arial"
    return chosen_font

def get_list_of_files(path_folder):
    """List usable files from input folder"""
    abs_path_folder = os.path.abspath(path_folder)
    file_paths = []
    if os.path.isdir(abs_path_folder):
        file_paths = [os.path.join(abs_path_folder, f) for f in os.listdir(abs_path_folder)]
    else:
        print("Failed to load input. Are you sure the path to ",
        "the directory is correct:\n",
        "\t", abs_path_folder)
    file_paths = [file for file in file_paths if file.endswith(".txt") and not file.endswith(".wrapped.txt")]
    return file_paths

def load_text_from_file(path):
    """Load content of a file if it exists"""
    text = False
    if os.path.exists(path):
        with open(path, "r", encoding="utf8") as fh:
            text = fh.read()
    return text 

def wrap_text(text, maxlen=50, flat=False, slice=0):
    """Wrap a text into lines never longer than maxlen"""
    if flat:
        text = text.lower()
    wrapped_text = wrap(text, maxlen)
    if slice < 1 or len(wrapped_text) < slice:
        return [wrapped_text]
    else:
        x = len(wrapped_text) // slice
        return [s.tolist() for s in np.array_split(wrapped_text, x)]

def save_wrapped_text(wrapped_text, path):
    """Save a list of strings in a file"""
    if wrapped_text:
        if len(wrapped_text) == 1:
            unwrapped_text = "\n".join(wrapped_text[0])
        elif len(wrapped_text) > 1:
            wrapped_text = ["\n".join(group) for group in wrapped_text]
            unwrapped_text = "\n==--==--==--==\n".join(wrapped_text)
        with open(path, "w+", encoding="utf8") as fh:
            fh.write(unwrapped_text)
    else:
        print("Something is wrong with the wrapped text.")

def create_pdf(wrapped_text, path_out, font=False): 
    """Create a PDF containing a series of text strings"""    
    pdf = FPDF()
    font_name = define_font(pdf, font)
    pdf.set_font(font_name, size=14)
    for group in wrapped_text:
        pdf.add_page() 
        for line in group:
            pdf.cell(50,10, txt=line, ln=1, align="L")
    pdf.output(path_out)

def create_output_basename(path):
    """Create path for output files"""
    basename = ".".join(os.path.basename(path).split(".")[:-1])
    return os.path.join(os.path.dirname(path), basename)


def main(input_path, font, merge):
    file_paths = get_list_of_files(input_path)
    merged_wraps = []
    for file in tqdm(file_paths):
        print("working with:", os.path.basename(file))
        output_name = create_output_basename(file)
        text = load_text_from_file(file)
        if text:
            wrapped_text = wrap_text(text, slice=20)  #20 lines max in each page
            if merge:
                merged_wraps += wrapped_text
            else:
                save_wrapped_text(wrapped_text, f"{output_name}.wrapped.txt")
                create_pdf(wrapped_text, f"{output_name}.pdf", font)
        else:
            print("no text")
    if merge:
        if input_path[-1] == os.sep:
            input_path = input_path[:-1]
        folder_name = input_path.split(os.sep)[-1]
        if merged_wraps:
            save_wrapped_text(merged_wraps, os.path.join(os.path.abspath(input_path),f"merged_{folder_name}_{font}.wrapped.txt"))
            create_pdf(merged_wraps, os.path.join(os.path.abspath(input_path),f"merged_{folder_name}_{font}.pdf"), font)


parser = argparse.ArgumentParser()
parser.add_argument("--test", help="execute on test files", action="store_true")
parser.add_argument("-i", "--input", nargs="?", help="path to input files")
parser.add_argument("-f", "--font", nargs="?", help="set font for PDF: [mono|sans|serif]")
parser.add_argument("--merge", action="store_true", help="merge all document and make only one PDF file")
args = parser.parse_args()

if args.test:
    main("test/", "sans", False)
else:
    if args.input:
        print(args)
        main(args.input, args.font, args.merge)
    else:
        print("You need to provide a path to some source files (-i/--input PATH)")

