# Expérience Wikipedia Vocabulaire

## The project
This repository has been created as part of an experience we are making to evaluate if a transcription model is making more errors on words not present in the ground truths.
To do so, we decided to created artificial ground truths by extracting the content of some [French Wikipedia](https://fr.wikipedia.org/wiki/Wikip%C3%A9dia:Accueil_principal) pages on the same topics: art (GT1) and astronomy (GT2).

## The environment
To work properly on the scripts and motsly on the one that allows us to extract content from the Wikipedia pages, some specific dependencies are needed. To help with that, there is an [environment file](https://gitlab.inria.fr/dh-projects/exp-wikipedia-vocabulaire/-/blob/main/environment.yml), with which it is possible to create the environment needed to use the scripts.

## The scripts
There are several scripts in the folder of the same name and they each have to be used on a specific step of the process.

### Creating the corpus: extracting content from Wikipedia
This will be done with the [download.py](https://gitlab.inria.fr/dh-projects/exp-wikipedia-vocabulaire/-/blob/main/scripts/download.py) script. Two methods can be used to extract the content:

- Method 1: The provided argument is the URL of the Wikipedia page that is requested
- Method 2: The provided argument is the title of the Wikipedia page that is requested

To help with that, and notably when the work involves numerous requests and doing it one by one is inconvenient, a [shell script](https://gitlab.inria.fr/dh-projects/exp-wikipedia-vocabulaire/-/blob/main/download.sh) is also present in this repository, with examples from the first and second methods. The command line to use this script will simply be:
`$ bash download.sh`

The output is already declared in the script, so it is not necessary to put it as an argument in the command line. An example of the kind of outputs obtained with this script is available in the [output](https://gitlab.inria.fr/dh-projects/exp-wikipedia-vocabulaire/-/tree/main/output) folder.

## Producing frequency lists: knowing the words and characters of the content
Firstly, before producing the lists, it is necessary to gather in a single file all the content of the text files.
This simple script will do that in a few steps: [retrieving_text_files_content.py](https://gitlab.inria.fr/dh-projects/exp-wikipedia-vocabulaire/-/blob/main/scripts/retrieving_text_files_content.py)

Once it is done, this file will be used to create the lists. Both lists (words and characters) will be produced from the same [script](https://gitlab.inria.fr/dh-projects/exp-wikipedia-vocabulaire/-/blob/main/scripts/frequency_lists.py) and the outputs are CSV files with one column as the words/characters and the other as the number of occurrences. The word list is sorted by number of occurences in descending order and the character list is sorted alphabetically.

## Creating the artificial ground truths: adding the content on eScriptorium
In order to help create those artificial ground truths faster, there is two scripts. Since the transcribed content of the GT is already available as they are artificial and to avoid adding the line one by one after the segmentation, those scripts will help reduce the process exponentially. 

After the segmentation is done on eScriptorium and the files have been exported (ALTO or PAGE), the following script will firstly retrieve the ID of every text line from each XML file: [retrieve_id.py](https://gitlab.inria.fr/dh-projects/exp-wikipedia-vocabulaire/-/blob/main/scripts/retrieve_id.py). Those ID will be printed in the terminal in the order they appear on the text.
Then, for example, it is possible to copy them in a spreadsheet and add the corresponding line from the text files; then, the spreadsheet is transformed into a Python dictionary where the key is the ID and the value is the content of the line.

This dictionary can then be used with the other script written for this step: [adding_content.py](https://gitlab.inria.fr/dh-projects/exp-wikipedia-vocabulaire/-/blob/main/scripts/adding_content.py). Once again, it works with ALTO or PAGE (the method not needed can be put between triple air quotes). The process is pretty simple: the script reads the dictionary and the XML file, it looks for the ID in each `TextLine` and once it finds one that matches the file, it will look for the tag where the content is put and will add the value of its corresponding key.

Once this is done, the files can be reimported into eScriptorium and the artificial ground truth should be ready (Warning: verify the transcription to make sure that the line numbering was right or some mix-ups could come up).
